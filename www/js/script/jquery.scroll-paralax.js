//EFECTO PARALAX
$(document).ready(function(){
	$(window).scroll(function(){
		$('.paralax').each(function(i,elem){
			var paralax_factor=10;//-$(this).offset().top
			var paralax_posicion=$(window).scrollTop()/paralax_factor;
			var paralax_config={};
				paralax_config['transition']='all 0s';
				paralax_config['background-position']='50% 0';
				paralax_config['background-size']='cover';
				paralax_config['background-repeat']='repeat';
				//paralax_config['background-attachment']='fixed';
					
			if(scroll_visible(this)){
				if($(this).data('movimiento')){ 
					paralax_posicion=$(window).scrollTop()/$(this).data('movimiento');}
				$(this).css(paralax_config);
				$(this).css({'background-position':'50% -'+paralax_posicion+'px'});
			}else{
			}
		});
	});
	
});