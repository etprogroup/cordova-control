//VALIDAR SCROLL VISIBLE
function scroll_visible(elemento) {
	var elemento= $(elemento);
	var docViewTop = $(window).scrollTop();
	var docViewBottom = docViewTop + $(window).height();
	var elemOffset = 0;
	var elemMAXview=100;//0
	
	if(elemento.data('offset')!=undefined){
		elemOffset = elemento.data('offset');
	}
	var elemTop = $(elemento).offset().top;
	var elemBottom = elemTop + $(elemento).height();
	
	if(elemOffset != 0) { 
		if(docViewTop - elemTop >= 0) {
		  elemTop = $(elemento).offset().top + elemOffset;
		} else {
		  elemBottom = elemTop + $(elemento).height() - elemOffset
		}
	}
	
	if(((elemBottom <= docViewBottom) || ((elemTop+elemMAXview) <= docViewBottom)) && ((elemTop >= docViewTop) || ((elemBottom-elemMAXview) >= docViewTop))){
		return true;
	}else{
		return false;
	}
}