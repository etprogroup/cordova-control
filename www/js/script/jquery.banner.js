//EFECTO DE MENSAJES
function banner_animacion(elemento){
	var default_efecto='left';
	var default_origen='150%';
	var default_destino='';
	var default_tiempo=1000;
	var default_espera=1000;

	//$(elemento).parents('.banner_seccion').css({'background-position':''}).animate({'backgroundPosition':'top right'},20000);
	//$(elemento).parents('.banner_seccion').css({'background-position':'','transition':'all 20s'}).css({'background-position':'top right'});
	$(elemento).find('.banner_animacion').each(function(index, element){
		var efecto=$(this).data('efecto');
		var tiempo=$(this).data('tiempo');
		var espera=$(this).data('espera');
		var origen=$(this).data('origen');
		var destino=$(this).data('destino');
		
		if(efecto){}else{efecto=default_efecto;}
		if(tiempo){}else{tiempo=default_tiempo;}
		if(origen){}else{origen=default_origen;}
		if(destino){}else{destino=default_destino;}
		if(espera){}else{espera=default_espera*Number($(this).index());}
		
		var attr_origen={};
			attr_origen['position']='relative';
			attr_origen['opacity']='0';
			attr_origen['transition']='all 1s';
			attr_origen[efecto]=origen;
		
		var attr_destino={};
			attr_destino['opacity']='1';
			attr_destino[efecto]='';
		
		$(this).css(attr_origen).delay(espera).animate(attr_destino,tiempo);
			
		if(efecto=='left'){
			//$(this).css({'left':'150%','opacity':'0'}).animate({'left':'','opacity':'1'},tiempo);
		}
		
	});
	
}